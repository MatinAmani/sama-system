﻿namespace Sama_System
{
    public class Lecture
    {
        private string Code;
        private string Name;
        private string Type;
        private int Unit;

        public Lecture(string code, string name, string type, int unit)
        {
            this.Code = code;
            this.Name = name;
            this.Type = type;
            this.Unit = unit;
        }

        public void Add_toDatabase()
        {
            DatabaseConnection.ManipulateData("insert into Lecture values('" + this.Code + "','" + this.Name + "','" + this.Type + "'," + this.Unit + ")");
        }

        public string Info()
        {
            string info;
            info = this.Code + "_" + this.Name + "_" + this.Type + "_" + this.Unit;
            return (info);
        }
    }
}
