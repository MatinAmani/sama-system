﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class SearchTeacherForm : Form
    {
        public SearchTeacherForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (Validation.ObjectExist(Code_TextBox.Text, "Teacher"))
            {
                Teacher teacher = DatabaseConnection.Teacher_RawData(Code_TextBox.Text);
                string[] info = teacher.Info().Split('_');
                FullName_Label.Text = "Name: " + info[1] + " " + info[2];
                Code_Label.Text = "Code: " + info[0];
                Degree_Label.Text = "Degree: " + info[3];
                Salary_Label.Text = "Salary: $" + info[4];
                DataTable dataTable = DatabaseConnection.ShowData("select * from Grades where TCode = '" + Code_TextBox.Text + "'");
                Grade_DataGridView.DataSource = dataTable.DefaultView;
            }
            else
            {
                MessageBox.Show("Teacher Code: " + Code_TextBox.Text + " Not Exist!", "Data Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
