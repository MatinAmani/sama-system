﻿namespace Sama_System
{
    public class Teacher : Human
    {
        private string Degree;
        private int Salary;

        public Teacher(string code, string name, string last_name, string degree, int salary)
        {
            this.Code = code;
            this.Name = name;
            this.LastName = last_name;
            this.Degree = degree;
            this.Salary = salary;
        }

        public void Add_toDatabase()
        {
            DatabaseConnection.ManipulateData("insert into Teacher values('" + this.Code + "','" + this.Name + "','" + this.LastName + "','" + this.Degree + "'," + this.Salary + ")");
        }

        public string Info()
        {
            string info;
            info = this.Code + "_" + this.Name + "_" + this.LastName + "_" + this.Degree + "_" + this.Salary;
            return (info);
        }
    }
}
