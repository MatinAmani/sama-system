﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class LoginForm : Form
    {
        public static string Uname;
        public LoginForm()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Login_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from Users where UserName=N'" + Username_TextBox.Text + "' and UserPass=N'" + Password_TextBox.Text + "'", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            if (sqlDataReader.Read())
            {
                string UserType = sqlDataReader[2].ToString();
                Uname = sqlDataReader[0].ToString();
                switch (UserType)
                {
                    case "Admin":
                        AdminPanel Admin = new AdminPanel();
                        this.Hide();
                        Admin.Show();
                        sqlConnection.Close();
                        break;

                    case "Teacher":
                        TeacherPanel Teacher = new TeacherPanel();
                        this.Hide();
                        Teacher.Show();
                        sqlConnection.Close();
                        break;

                    case "Student":
                        StudentPanel Student = new StudentPanel();
                        this.Hide();
                        Student.Show();
                        sqlConnection.Close();
                        break;
                }
            }
            else
            {
                MessageBox.Show("Username or Password is Incorrect", "Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                sqlConnection.Close();
            }
        }

        private void LoginForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            Password_TextBox.Text = "";
        }
    }
}
