﻿namespace Sama_System
{
    partial class SearchTeacherForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchTeacherForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.Search_Panel = new System.Windows.Forms.Panel();
            this.Records_Panel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Code_TextBox = new System.Windows.Forms.TextBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.Degree_Label = new System.Windows.Forms.Label();
            this.Code_Label = new System.Windows.Forms.Label();
            this.FullName_Label = new System.Windows.Forms.Label();
            this.Salary_Label = new System.Windows.Forms.Label();
            this.Grade_DataGridView = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.Search_Panel.SuspendLayout();
            this.Records_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grade_DataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Location = new System.Drawing.Point(508, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(100, 100);
            this.panel1.TabIndex = 0;
            // 
            // Search_Panel
            // 
            this.Search_Panel.Controls.Add(this.CancelButton);
            this.Search_Panel.Controls.Add(this.SearchButton);
            this.Search_Panel.Controls.Add(this.Code_TextBox);
            this.Search_Panel.Controls.Add(this.label1);
            this.Search_Panel.Location = new System.Drawing.Point(12, 12);
            this.Search_Panel.Name = "Search_Panel";
            this.Search_Panel.Size = new System.Drawing.Size(470, 100);
            this.Search_Panel.TabIndex = 1;
            // 
            // Records_Panel
            // 
            this.Records_Panel.Controls.Add(this.label2);
            this.Records_Panel.Controls.Add(this.Grade_DataGridView);
            this.Records_Panel.Controls.Add(this.Salary_Label);
            this.Records_Panel.Controls.Add(this.Degree_Label);
            this.Records_Panel.Controls.Add(this.Code_Label);
            this.Records_Panel.Controls.Add(this.FullName_Label);
            this.Records_Panel.Location = new System.Drawing.Point(12, 118);
            this.Records_Panel.Name = "Records_Panel";
            this.Records_Panel.Size = new System.Drawing.Size(596, 270);
            this.Records_Panel.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Teacher Code:";
            // 
            // Code_TextBox
            // 
            this.Code_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Code_TextBox.Location = new System.Drawing.Point(163, 35);
            this.Code_TextBox.Name = "Code_TextBox";
            this.Code_TextBox.Size = new System.Drawing.Size(211, 31);
            this.Code_TextBox.TabIndex = 1;
            // 
            // SearchButton
            // 
            this.SearchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchButton.Location = new System.Drawing.Point(380, 16);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(87, 31);
            this.SearchButton.TabIndex = 2;
            this.SearchButton.Text = "Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelButton.Location = new System.Drawing.Point(380, 53);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(87, 31);
            this.CancelButton.TabIndex = 3;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // Degree_Label
            // 
            this.Degree_Label.AutoSize = true;
            this.Degree_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Degree_Label.Location = new System.Drawing.Point(3, 53);
            this.Degree_Label.Name = "Degree_Label";
            this.Degree_Label.Size = new System.Drawing.Size(88, 25);
            this.Degree_Label.TabIndex = 8;
            this.Degree_Label.Text = "Degree:";
            // 
            // Code_Label
            // 
            this.Code_Label.AutoSize = true;
            this.Code_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Code_Label.Location = new System.Drawing.Point(306, 15);
            this.Code_Label.Name = "Code_Label";
            this.Code_Label.Size = new System.Drawing.Size(69, 25);
            this.Code_Label.TabIndex = 7;
            this.Code_Label.Text = "Code:";
            // 
            // FullName_Label
            // 
            this.FullName_Label.AutoSize = true;
            this.FullName_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName_Label.Location = new System.Drawing.Point(3, 15);
            this.FullName_Label.Name = "FullName_Label";
            this.FullName_Label.Size = new System.Drawing.Size(74, 25);
            this.FullName_Label.TabIndex = 6;
            this.FullName_Label.Text = "Name:";
            // 
            // Salary_Label
            // 
            this.Salary_Label.AutoSize = true;
            this.Salary_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Salary_Label.Location = new System.Drawing.Point(306, 53);
            this.Salary_Label.Name = "Salary_Label";
            this.Salary_Label.Size = new System.Drawing.Size(79, 25);
            this.Salary_Label.TabIndex = 9;
            this.Salary_Label.Text = "Salary:";
            // 
            // Grade_DataGridView
            // 
            this.Grade_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grade_DataGridView.Location = new System.Drawing.Point(0, 118);
            this.Grade_DataGridView.Name = "Grade_DataGridView";
            this.Grade_DataGridView.ReadOnly = true;
            this.Grade_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grade_DataGridView.Size = new System.Drawing.Size(596, 152);
            this.Grade_DataGridView.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(199, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "Grades set by this teacher:";
            // 
            // SearchTeacherForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(620, 400);
            this.Controls.Add(this.Records_Panel);
            this.Controls.Add(this.Search_Panel);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SearchTeacherForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search Teachers";
            this.Search_Panel.ResumeLayout(false);
            this.Search_Panel.PerformLayout();
            this.Records_Panel.ResumeLayout(false);
            this.Records_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grade_DataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel Search_Panel;
        private System.Windows.Forms.Panel Records_Panel;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.TextBox Code_TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Salary_Label;
        private System.Windows.Forms.Label Degree_Label;
        private System.Windows.Forms.Label Code_Label;
        private System.Windows.Forms.Label FullName_Label;
        private System.Windows.Forms.DataGridView Grade_DataGridView;
        private System.Windows.Forms.Label label2;
    }
}