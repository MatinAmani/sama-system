﻿namespace Sama_System
{
    partial class SearchLectureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchLectureForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.Search_Panel = new System.Windows.Forms.Panel();
            this.Records_Panel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Code_TextBox = new System.Windows.Forms.TextBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.Name_Label = new System.Windows.Forms.Label();
            this.Code_Label = new System.Windows.Forms.Label();
            this.LectureType_Label = new System.Windows.Forms.Label();
            this.Unit_Label = new System.Windows.Forms.Label();
            this.Grade_DataGridView = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.Search_Panel.SuspendLayout();
            this.Records_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grade_DataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Location = new System.Drawing.Point(488, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(100, 100);
            this.panel1.TabIndex = 0;
            // 
            // Search_Panel
            // 
            this.Search_Panel.Controls.Add(this.CancelButton);
            this.Search_Panel.Controls.Add(this.SearchButton);
            this.Search_Panel.Controls.Add(this.Code_TextBox);
            this.Search_Panel.Controls.Add(this.label1);
            this.Search_Panel.Location = new System.Drawing.Point(12, 12);
            this.Search_Panel.Name = "Search_Panel";
            this.Search_Panel.Size = new System.Drawing.Size(470, 100);
            this.Search_Panel.TabIndex = 1;
            // 
            // Records_Panel
            // 
            this.Records_Panel.Controls.Add(this.label2);
            this.Records_Panel.Controls.Add(this.Grade_DataGridView);
            this.Records_Panel.Controls.Add(this.Unit_Label);
            this.Records_Panel.Controls.Add(this.LectureType_Label);
            this.Records_Panel.Controls.Add(this.Code_Label);
            this.Records_Panel.Controls.Add(this.Name_Label);
            this.Records_Panel.Location = new System.Drawing.Point(12, 118);
            this.Records_Panel.Name = "Records_Panel";
            this.Records_Panel.Size = new System.Drawing.Size(576, 270);
            this.Records_Panel.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Lecture Code:";
            // 
            // Code_TextBox
            // 
            this.Code_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Code_TextBox.Location = new System.Drawing.Point(170, 35);
            this.Code_TextBox.Name = "Code_TextBox";
            this.Code_TextBox.Size = new System.Drawing.Size(186, 31);
            this.Code_TextBox.TabIndex = 4;
            // 
            // SearchButton
            // 
            this.SearchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchButton.Location = new System.Drawing.Point(362, 16);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(105, 31);
            this.SearchButton.TabIndex = 5;
            this.SearchButton.Text = "Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelButton.Location = new System.Drawing.Point(362, 53);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(105, 31);
            this.CancelButton.TabIndex = 6;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // Name_Label
            // 
            this.Name_Label.AutoSize = true;
            this.Name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_Label.Location = new System.Drawing.Point(3, 21);
            this.Name_Label.Name = "Name_Label";
            this.Name_Label.Size = new System.Drawing.Size(59, 25);
            this.Name_Label.TabIndex = 4;
            this.Name_Label.Text = "Title:";
            // 
            // Code_Label
            // 
            this.Code_Label.AutoSize = true;
            this.Code_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Code_Label.Location = new System.Drawing.Point(297, 21);
            this.Code_Label.Name = "Code_Label";
            this.Code_Label.Size = new System.Drawing.Size(69, 25);
            this.Code_Label.TabIndex = 5;
            this.Code_Label.Text = "Code:";
            // 
            // LectureType_Label
            // 
            this.LectureType_Label.AutoSize = true;
            this.LectureType_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LectureType_Label.Location = new System.Drawing.Point(3, 56);
            this.LectureType_Label.Name = "LectureType_Label";
            this.LectureType_Label.Size = new System.Drawing.Size(144, 25);
            this.LectureType_Label.TabIndex = 6;
            this.LectureType_Label.Text = "Lecture Type:";
            // 
            // Unit_Label
            // 
            this.Unit_Label.AutoSize = true;
            this.Unit_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Unit_Label.Location = new System.Drawing.Point(297, 56);
            this.Unit_Label.Name = "Unit_Label";
            this.Unit_Label.Size = new System.Drawing.Size(67, 25);
            this.Unit_Label.TabIndex = 7;
            this.Unit_Label.Text = "Units:";
            // 
            // Grade_DataGridView
            // 
            this.Grade_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grade_DataGridView.Location = new System.Drawing.Point(0, 126);
            this.Grade_DataGridView.Name = "Grade_DataGridView";
            this.Grade_DataGridView.ReadOnly = true;
            this.Grade_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grade_DataGridView.Size = new System.Drawing.Size(576, 144);
            this.Grade_DataGridView.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 18);
            this.label2.TabIndex = 9;
            this.label2.Text = "Grades in this lecture:";
            // 
            // SearchLectureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(600, 400);
            this.Controls.Add(this.Records_Panel);
            this.Controls.Add(this.Search_Panel);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SearchLectureForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search Lectures";
            this.Search_Panel.ResumeLayout(false);
            this.Search_Panel.PerformLayout();
            this.Records_Panel.ResumeLayout(false);
            this.Records_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grade_DataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel Search_Panel;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.TextBox Code_TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Records_Panel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView Grade_DataGridView;
        private System.Windows.Forms.Label Unit_Label;
        private System.Windows.Forms.Label LectureType_Label;
        private System.Windows.Forms.Label Code_Label;
        private System.Windows.Forms.Label Name_Label;
    }
}