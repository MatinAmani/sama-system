﻿using System;
using System.Windows.Forms;

namespace Sama_System
{
    public static class Program
    {
        public static LoginForm loginForm = new LoginForm();

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(loginForm);
        }
    }
}
