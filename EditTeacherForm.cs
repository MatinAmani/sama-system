﻿using System;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class EditTeacherForm : Form
    {
        public EditTeacherForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (Validation.ObjectExist(Code_TextBox.Text, "Teacher"))
            {
                EditButton.Enabled = true;
                DeleteButton.Enabled = true;
                DiscardButton.Enabled = true;
                Name_TextBox.Enabled = false;
                LastName_TextBox.Enabled = false;
                CodeRslt_TextBox.Enabled = false;
                Degree_ComboBox.Enabled = false;
                Salary_TextBox.Enabled = false;
                Teacher teacher = DatabaseConnection.Teacher_RawData(Code_TextBox.Text);
                string[] info = teacher.Info().Split('_');
                CodeRslt_TextBox.Text = info[0];
                Name_TextBox.Text = info[1];
                LastName_TextBox.Text = info[2];
                Degree_ComboBox.Text = info[3];
                Salary_TextBox.Text = info[4];
            }
            else
            {
                MessageBox.Show("Teacher Code: " + Code_TextBox.Text + " Does Not Exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            SaveButton.Enabled = true;
            Name_TextBox.Enabled = true;
            LastName_TextBox.Enabled = true;
            CodeRslt_TextBox.Enabled = true;
            Degree_ComboBox.Enabled = true;
            Salary_TextBox.Enabled = true;
        }

        private void DiscardButton_Click(object sender, EventArgs e)
        {
            DialogResult rslt = MessageBox.Show("Discard Changes?", "Unsaved Settings", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (rslt == DialogResult.Yes)
            {
                SaveButton.Enabled = false;
                Name_TextBox.Enabled = false;
                LastName_TextBox.Enabled = false;
                CodeRslt_TextBox.Enabled = false;
                Degree_ComboBox.Enabled = false;
                Salary_TextBox.Enabled = false;
                Teacher teacher = DatabaseConnection.Teacher_RawData(Code_TextBox.Text);
                string[] info = teacher.Info().Split('_');
                CodeRslt_TextBox.Text = info[0];
                Name_TextBox.Text = info[1];
                LastName_TextBox.Text = info[2];
                Degree_ComboBox.Text = info[3];
                Salary_TextBox.Text = info[4];
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            DialogResult rslt = MessageBox.Show("Are You Sure You Want to Delete Teacher Code: " + Code_TextBox.Text + "?\nGrades Table Will be Effected!", "Student Will be Deleted", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (rslt == DialogResult.Yes)
            {
                DatabaseConnection.ManipulateData("delete from Grades where TCode='" + Code_TextBox.Text + "'");
                DatabaseConnection.ManipulateData("delete from Teacher where Code='" + Code_TextBox.Text + "'");
                EditButton.Enabled = false;
                DeleteButton.Enabled = false;
                SaveButton.Enabled = false;
                DiscardButton.Enabled = false;
                Code_TextBox.Text = "";
                Name_TextBox.Text = "";
                LastName_TextBox.Text = "";
                CodeRslt_TextBox.Text = "";
                Degree_ComboBox.Text = "";
                Salary_TextBox.Text = "";
                MessageBox.Show("Teacher Deleted Successfuly!", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (Validation.CodeLength(CodeRslt_TextBox.Text, 10))
            {
                DatabaseConnection.ManipulateData("update Teacher set Code='" + CodeRslt_TextBox.Text + "', Name='" + Name_TextBox.Text + "', LastName='" + LastName_TextBox.Text + "', Degree='" + Degree_ComboBox.SelectedItem.ToString() + "', Salary=" + int.Parse(Salary_TextBox.Text) + " where Code='" + Code_TextBox.Text + "'");
                MessageBox.Show("Records Updated", "Done!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                EditButton.Enabled = false;
                DeleteButton.Enabled = false;
                SaveButton.Enabled = false;
                DiscardButton.Enabled = false;
                Name_TextBox.Enabled = false;
                LastName_TextBox.Enabled = false;
                CodeRslt_TextBox.Enabled = false;
                Degree_ComboBox.Enabled = false;
                Salary_TextBox.Enabled = false;
                Code_TextBox.Text = "";
                Name_TextBox.Text = "";
                LastName_TextBox.Text = "";
                CodeRslt_TextBox.Text = "";
                Degree_ComboBox.Text = "";
                Salary_TextBox.Text = "";
            }
            else
            {
                MessageBox.Show("Code Must be 10 Digits.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CodeRslt_TextBox.Text = Code_TextBox.Text;
            }
        }
    }
}
