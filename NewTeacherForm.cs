﻿using System;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class NewTeacherForm : Form
    {
        public NewTeacherForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (Validation.CodeUniqueness(Code_TextBox.Text, "Teacher"))
            {
                if (Validation.CodeLength(Code_TextBox.Text, 10))
                {
                    if (!Validation.Null(Degree_ComboBox.Text) && !Validation.Null(Salary_TextBox.Text))
                    {
                        Teacher teacher = new Teacher(Code_TextBox.Text, Name_TextBox.Text, LastName_TextBox.Text, Degree_ComboBox.SelectedItem.ToString(), int.Parse(Salary_TextBox.Text));
                        teacher.Add_toDatabase();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Please Fill All The Fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    DialogResult rslt = MessageBox.Show("Code Must be 10 Digits!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (rslt == DialogResult.OK)
                    {
                        Code_TextBox.Text = "";
                    }
                }
            }
            else
            {
                string str = Code_TextBox.Text;
                DialogResult rslt = MessageBox.Show("Teacher With Code: " + str + " Already Exists!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (rslt == DialogResult.OK)
                {
                    Name_TextBox.Text = "";
                    LastName_TextBox.Text = "";
                    Code_TextBox.Text = "";
                    Degree_ComboBox.Text = "";
                    Salary_TextBox.Text = "";
                }
            }
        }

        private void Code_TextBox_TextChanged(object sender, EventArgs e)
        {
            string value = Code_TextBox.Text;
            if (!Validation.CodeUniqueness(value, "Teacher"))
            {
                MessageBox.Show("Teacher With Code: " + value + " Already Exists!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (!Validation.MaxLength(value, 10))
            {
                MessageBox.Show("Code Must be 10 Digits!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
