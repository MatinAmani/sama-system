﻿namespace Sama_System
{
    partial class TeacherPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addScoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scoreAStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newLectureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.amongStudentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.amongTeachersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.amongLecturesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Username_StripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Student_GridView = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Lecture_DataGridView = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Student_GridView)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Lecture_DataGridView)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addScoreToolStripMenuItem,
            this.addToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.accountToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(925, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addScoreToolStripMenuItem
            // 
            this.addScoreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scoreAStudentToolStripMenuItem});
            this.addScoreToolStripMenuItem.Name = "addScoreToolStripMenuItem";
            this.addScoreToolStripMenuItem.Size = new System.Drawing.Size(114, 24);
            this.addScoreToolStripMenuItem.Text = "Score Options";
            // 
            // scoreAStudentToolStripMenuItem
            // 
            this.scoreAStudentToolStripMenuItem.Name = "scoreAStudentToolStripMenuItem";
            this.scoreAStudentToolStripMenuItem.Size = new System.Drawing.Size(182, 24);
            this.scoreAStudentToolStripMenuItem.Text = "Score a Student";
            this.scoreAStudentToolStripMenuItem.Click += new System.EventHandler(this.scoreAStudentToolStripMenuItem_Click);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newStudentToolStripMenuItem,
            this.newLectureToolStripMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(49, 24);
            this.addToolStripMenuItem.Text = "Add";
            // 
            // newStudentToolStripMenuItem
            // 
            this.newStudentToolStripMenuItem.Name = "newStudentToolStripMenuItem";
            this.newStudentToolStripMenuItem.Size = new System.Drawing.Size(163, 24);
            this.newStudentToolStripMenuItem.Text = "New Student";
            this.newStudentToolStripMenuItem.Click += new System.EventHandler(this.newStudentToolStripMenuItem_Click);
            // 
            // newLectureToolStripMenuItem
            // 
            this.newLectureToolStripMenuItem.Name = "newLectureToolStripMenuItem";
            this.newLectureToolStripMenuItem.Size = new System.Drawing.Size(163, 24);
            this.newLectureToolStripMenuItem.Text = "New Lecture";
            this.newLectureToolStripMenuItem.Click += new System.EventHandler(this.newLectureToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.amongStudentsToolStripMenuItem,
            this.amongTeachersToolStripMenuItem,
            this.amongLecturesToolStripMenuItem});
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(65, 24);
            this.searchToolStripMenuItem.Text = "Search";
            // 
            // amongStudentsToolStripMenuItem
            // 
            this.amongStudentsToolStripMenuItem.Name = "amongStudentsToolStripMenuItem";
            this.amongStudentsToolStripMenuItem.Size = new System.Drawing.Size(188, 24);
            this.amongStudentsToolStripMenuItem.Text = "Among Students";
            this.amongStudentsToolStripMenuItem.Click += new System.EventHandler(this.amongStudentsToolStripMenuItem_Click);
            // 
            // amongTeachersToolStripMenuItem
            // 
            this.amongTeachersToolStripMenuItem.Name = "amongTeachersToolStripMenuItem";
            this.amongTeachersToolStripMenuItem.Size = new System.Drawing.Size(188, 24);
            this.amongTeachersToolStripMenuItem.Text = "Among Teachers";
            this.amongTeachersToolStripMenuItem.Click += new System.EventHandler(this.amongTeachersToolStripMenuItem_Click);
            // 
            // amongLecturesToolStripMenuItem
            // 
            this.amongLecturesToolStripMenuItem.Name = "amongLecturesToolStripMenuItem";
            this.amongLecturesToolStripMenuItem.Size = new System.Drawing.Size(188, 24);
            this.amongLecturesToolStripMenuItem.Text = "Among Lectures";
            this.amongLecturesToolStripMenuItem.Click += new System.EventHandler(this.amongLecturesToolStripMenuItem_Click);
            // 
            // accountToolStripMenuItem
            // 
            this.accountToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Username_StripMenu,
            this.changePasswordToolStripMenuItem,
            this.logOutToolStripMenuItem});
            this.accountToolStripMenuItem.Name = "accountToolStripMenuItem";
            this.accountToolStripMenuItem.Size = new System.Drawing.Size(75, 24);
            this.accountToolStripMenuItem.Text = "Account";
            // 
            // Username_StripMenu
            // 
            this.Username_StripMenu.Enabled = false;
            this.Username_StripMenu.Name = "Username_StripMenu";
            this.Username_StripMenu.Size = new System.Drawing.Size(193, 24);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.changePasswordToolStripMenuItem.Text = "Change Password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Student_GridView);
            this.panel1.Location = new System.Drawing.Point(11, 99);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(449, 350);
            this.panel1.TabIndex = 1;
            // 
            // Student_GridView
            // 
            this.Student_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Student_GridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Student_GridView.Location = new System.Drawing.Point(0, 0);
            this.Student_GridView.Name = "Student_GridView";
            this.Student_GridView.ReadOnly = true;
            this.Student_GridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Student_GridView.Size = new System.Drawing.Size(449, 350);
            this.Student_GridView.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Lecture_DataGridView);
            this.panel2.Location = new System.Drawing.Point(466, 99);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(447, 350);
            this.panel2.TabIndex = 2;
            // 
            // Lecture_DataGridView
            // 
            this.Lecture_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Lecture_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Lecture_DataGridView.Location = new System.Drawing.Point(0, 0);
            this.Lecture_DataGridView.Name = "Lecture_DataGridView";
            this.Lecture_DataGridView.ReadOnly = true;
            this.Lecture_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Lecture_DataGridView.Size = new System.Drawing.Size(447, 350);
            this.Lecture_DataGridView.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.DodgerBlue;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(11, 27);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(449, 66);
            this.panel3.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(171, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "Students";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Lime;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(466, 27);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(447, 66);
            this.panel4.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(171, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 29);
            this.label2.TabIndex = 6;
            this.label2.Text = "Lectures";
            // 
            // Timer
            // 
            this.Timer.Enabled = true;
            this.Timer.Interval = 2000;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // TeacherPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 461);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "TeacherPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Teachers Panel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TeacherPanel_FormClosing);
            this.Load += new System.EventHandler(this.TeacherPanel_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Student_GridView)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Lecture_DataGridView)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView Student_GridView;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView Lecture_DataGridView;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.ToolStripMenuItem addScoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scoreAStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newStudentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newLectureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem amongStudentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem amongTeachersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem amongLecturesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Username_StripMenu;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
    }
}