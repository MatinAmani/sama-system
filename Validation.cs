﻿using System.Configuration;
using System.Data.SqlClient;

namespace Sama_System
{
    public static class Validation
    {
        public static bool UsernameUniqueness(string username)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["Constr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from Users where UserName=N'" + username + "'", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            if (sqlDataReader.Read())
            {
                sqlConnection.Close();
                return (false);
            }
            else
            {
                sqlConnection.Close();
                return (true);
            }
        }

        public static bool CodeExist(string code, string area)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from " + area + " where Code='" + code + "'", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            if (sqlDataReader.Read())
            {
                sqlConnection.Close();
                return (true);
            }
            else
            {
                sqlConnection.Close();
                return (false);
            }
        }

        public static bool CodeUniqueness(string code, string area)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from " + area + " where Code='" + code + "'", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            if (sqlDataReader.Read())
            {
                sqlConnection.Close();
                return (false);
            }
            else
            {
                sqlConnection.Close();
                return (true);
            }
        }

        public static bool MaxLength(string value, int validation)
        {
            return (value.Length <= validation);
        }

        public static bool MinLength(string value, int validation)
        {
            return (value.Length >= validation);
        }

        public static bool CodeLength(string value, int validation)
        {
            return (value.Length == validation);
        }

        public static bool MaxValue(int value, int validation)
        {
            return (value <= validation);
        }

        public static bool MinValue(int value, int validation)
        {
            return (value >= validation);
        }

        public static bool UnitValidation(int unit, int min, int max)
        {
            return (unit >= min && unit <= max);
        }

        public static bool Null(string value)
        {
            return (value.Length == 0);
        }

        public static bool PasswordConfirm(string pass, string confirm)
        {
            return (pass == confirm);
        }

        public static bool ScoreExist(string stcode, string lctcode, int semester)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from Grades where StCode='" + stcode + "' and LctCode='" + lctcode + "' and Semester=" + semester, sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            if (sqlDataReader.Read())
            {
                sqlConnection.Close();
                return (true);
            }
            else
            {
                sqlConnection.Close();
                return (false);
            }
        }

        public static bool ObjectExist(string code, string obj)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from " + obj + " where Code='" + code + "'", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            if (sqlDataReader.Read())
            {
                sqlConnection.Close();
                return (true);
            }
            else
            {
                sqlConnection.Close();
                return (false);
            }
        }

        public static bool UserExist(string username)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from Users where UserName='" + username + "'", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            if (sqlDataReader.Read())
            {
                sqlConnection.Close();
                return (true);
            }
            else
            {
                sqlConnection.Close();
                return (false);
            }
        }

        public static bool ScoreValidation(float score)
        {
            return (score >= 0 && score <= 20);
        }
    }
}
