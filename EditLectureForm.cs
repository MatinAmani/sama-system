﻿using System;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class EditLectureForm : Form
    {
        public EditLectureForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (Validation.ObjectExist(Code_TextBox.Text, "Lecture"))
            {
                EditButton.Enabled = true;
                DeleteButton.Enabled = true;
                DiscardButton.Enabled = true;
                Name_TextBox.Enabled = false;
                CodeRslt_TextBox.Enabled = false;
                LectureType_ComboBox.Enabled = false;
                Unit_TextBox.Enabled = false;
                Lecture lecture = DatabaseConnection.Lecture_RawData(Code_TextBox.Text);
                string[] info = lecture.Info().Split('_');
                CodeRslt_TextBox.Text = info[0];
                Name_TextBox.Text = info[1];
                LectureType_ComboBox.Text = info[2];
                Unit_TextBox.Text = info[3];
            }
            else
            {
                MessageBox.Show("Lecture Code: " + Code_TextBox.Text + " Does Not Exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            SaveButton.Enabled = true;
            Name_TextBox.Enabled = true;
            CodeRslt_TextBox.Enabled = true;
            LectureType_ComboBox.Enabled = true;
            Unit_TextBox.Enabled = true;
        }

        private void DiscardButton_Click(object sender, EventArgs e)
        {
            DialogResult rslt = MessageBox.Show("Discard Changes?", "Unsaved Settings", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (rslt == DialogResult.Yes)
            {
                SaveButton.Enabled = false;
                Name_TextBox.Enabled = false;
                CodeRslt_TextBox.Enabled = false;
                LectureType_ComboBox.Enabled = false;
                Unit_TextBox.Enabled = false;
                Lecture lecture = DatabaseConnection.Lecture_RawData(Code_TextBox.Text);
                string[] info = lecture.Info().Split('_');
                CodeRslt_TextBox.Text = info[0];
                Name_TextBox.Text = info[1];
                LectureType_ComboBox.Text = info[2];
                Unit_TextBox.Text = info[3];
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (Validation.CodeLength(CodeRslt_TextBox.Text, 10))
            {
                DatabaseConnection.ManipulateData("update Lecture set Code='" + CodeRslt_TextBox.Text + "', Name='" + Name_TextBox.Text + "', Type='" + LectureType_ComboBox.SelectedItem.ToString() + "', Unit=" + int.Parse(Unit_TextBox.Text) + " where Code='" + Code_TextBox.Text + "'");
                MessageBox.Show("Records Updated", "Done!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                EditButton.Enabled = false;
                DeleteButton.Enabled = false;
                SaveButton.Enabled = false;
                DiscardButton.Enabled = false;
                Name_TextBox.Enabled = false;
                CodeRslt_TextBox.Enabled = false;
                LectureType_ComboBox.Enabled = false;
                Unit_TextBox.Enabled = false;
                Code_TextBox.Text = "";
                CodeRslt_TextBox.Text = "";
                Name_TextBox.Text = "";
                LectureType_ComboBox.Text = "";
                Unit_TextBox.Text = "";
            }
            else
            {
                MessageBox.Show("Code Must be 10 Digits.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CodeRslt_TextBox.Text = Code_TextBox.Text;
            }
        }
    }
}
