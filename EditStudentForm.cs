﻿using System;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class EditStudentForm : Form
    {
        public EditStudentForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (Validation.ObjectExist(Code_TextBox.Text, "Student"))
            {
                EditButton.Enabled = true;
                DeleteButton.Enabled = true;
                DiscardButton.Enabled = true;
                Name_TextBox.Enabled = false;
                LastName_TextBox.Enabled = false;
                CodeRslt_TextBox.Enabled = false;
                Field_ComboBox.Enabled = false;
                Student student = DatabaseConnection.Student_RawData(Code_TextBox.Text);
                string[] info = student.Info().Split('_');
                CodeRslt_TextBox.Text = info[0];
                Name_TextBox.Text = info[1];
                LastName_TextBox.Text = info[2];
                Field_ComboBox.Text = info[3];
            }
            else
            {
                MessageBox.Show("Student Code: " + Code_TextBox.Text + " Does Not Exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DiscardButton_Click(object sender, EventArgs e)
        {
            DialogResult rslt = MessageBox.Show("Discard Changes?", "Unsaved Settings", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (rslt == DialogResult.Yes)
            {
                Name_TextBox.Enabled = false;
                LastName_TextBox.Enabled = false;
                CodeRslt_TextBox.Enabled = false;
                Field_ComboBox.Enabled = false;
                Student student = DatabaseConnection.Student_RawData(Code_TextBox.Text);
                string[] info = student.Info().Split('_');
                CodeRslt_TextBox.Text = info[0];
                Name_TextBox.Text = info[1];
                LastName_TextBox.Text = info[2];
                Field_ComboBox.Text = info[3];
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            SaveButton.Enabled = true;
            Name_TextBox.Enabled = true;
            LastName_TextBox.Enabled = true;
            CodeRslt_TextBox.Enabled = true;
            Field_ComboBox.Enabled = true;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            DialogResult rslt = MessageBox.Show("Are You Sure You Want to Delete Student Code: " + Code_TextBox.Text + "?\nGrades and Users Table Will be Effected!", "Student Will be Deleted", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (rslt == DialogResult.Yes)
            {
                DatabaseConnection.ManipulateData("delete from Grades where StCode='" + Code_TextBox.Text + "'");
                DatabaseConnection.ManipulateData("delete from Users where UserName='" + Code_TextBox.Text + "'");
                DatabaseConnection.ManipulateData("delete from Student where Code='" + Code_TextBox.Text + "'");
                EditButton.Enabled = false;
                DeleteButton.Enabled = false;
                SaveButton.Enabled = false;
                DiscardButton.Enabled = false;
                Code_TextBox.Text = "";
                Name_TextBox.Text = "";
                LastName_TextBox.Text = "";
                CodeRslt_TextBox.Text = "";
                Field_ComboBox.Text = "";
                MessageBox.Show("Student Deleted Successfuly!", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (Validation.MaxLength(CodeRslt_TextBox.Text, 10))
            {
                DatabaseConnection.ManipulateData("update Student set Code='" + CodeRslt_TextBox.Text + "', Name='" + Name_TextBox.Text + "', LastName='" + LastName_TextBox.Text + "', Field='" + Field_ComboBox.SelectedItem.ToString() + "' where Code='" + Code_TextBox.Text + "'");
                MessageBox.Show("Records Updated", "Done!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                EditButton.Enabled = false;
                DeleteButton.Enabled = false;
                SaveButton.Enabled = false;
                DiscardButton.Enabled = false;
                Name_TextBox.Enabled = false;
                LastName_TextBox.Enabled = false;
                CodeRslt_TextBox.Enabled = false;
                Field_ComboBox.Enabled = false;
                Code_TextBox.Text = "";
                Name_TextBox.Text = "";
                LastName_TextBox.Text = "";
                CodeRslt_TextBox.Text = "";
                Field_ComboBox.Text = "";
            }
            else
            {
                MessageBox.Show("Code Must be 10 Digits or Less.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CodeRslt_TextBox.Text = Code_TextBox.Text;
            }
        }
    }
}
