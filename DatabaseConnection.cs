﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Sama_System
{
    public static class DatabaseConnection
    {
        private static string Query;

        public static DataTable ShowData(string str)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            Query = str;
            SqlDataAdapter dataAdapter = new SqlDataAdapter(Query, sqlConnection);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            sqlConnection.Close();
            return (dataTable);
        }

        public static void ManipulateData(string str)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            Query = str;
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = Query;
            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch
            {
                MessageBox.Show("SQL Command Did Not Executed Successfuly.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            sqlConnection.Close();
        }

        public static void BackupData(string path)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            Query = "backup database University to disk='" + path + "'";
            SqlCommand sqlCommand = new SqlCommand(Query, sqlConnection);
            try
            {
                sqlCommand.ExecuteNonQuery();
                MessageBox.Show("Backup Successful.", "Query Executed", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("SQL Command Did Not Executed Successfuly.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            sqlConnection.Close();
        }

        public static void RestoreData(string path)
        {
            SqlConnection sqlConnection = new SqlConnection("server=(local);database=;integrated security=true;");
            sqlConnection.Open();
            Query = "use master alter database University set single_user with rollback immediate" +
                " restore database University from disk='" + path + "' with replace";
            SqlCommand sqlCommand = new SqlCommand(Query, sqlConnection);
            try
            {
                sqlCommand.ExecuteNonQuery();
                MessageBox.Show("Backup Restored Successfuly.", "Query Executed!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("SQL Command Did Not Executed Successfuly.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            sqlConnection.Close();
        }

        public static Student Student_RawData(string code)
        {
            Student student;
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from Student where Code='" + code + "'", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            sqlDataReader.Read();
            student = new Student(sqlDataReader[0].ToString(), sqlDataReader[1].ToString(), sqlDataReader[2].ToString(), sqlDataReader[3].ToString());
            sqlConnection.Close();
            return (student);
        }

        public static Teacher Teacher_RawData(string code)
        {
            Teacher teacher;
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from Teacher where Code='" + code + "'", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            sqlDataReader.Read();
            teacher = new Teacher(sqlDataReader[0].ToString(), sqlDataReader[1].ToString(), sqlDataReader[2].ToString(), sqlDataReader[3].ToString(), int.Parse(sqlDataReader[4].ToString()));
            sqlConnection.Close();
            return (teacher);
        }

        public static Lecture Lecture_RawData(string code)
        {
            Lecture lecture;
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from Lecture where Code='" + code + "'", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            sqlDataReader.Read();
            lecture = new Lecture(sqlDataReader[0].ToString(), sqlDataReader[1].ToString(), sqlDataReader[2].ToString(), int.Parse(sqlDataReader[3].ToString()));
            sqlConnection.Close();
            return (lecture);
        }

        public static User User_RawData(string username)
        {
            User user;
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from Users where UserName='" + username + "'", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            sqlDataReader.Read();
            user = new User(sqlDataReader[0].ToString(), sqlDataReader[1].ToString(), sqlDataReader[2].ToString());
            sqlConnection.Close();
            return (user);
        }
    }
}
