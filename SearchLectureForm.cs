﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class SearchLectureForm : Form
    {
        public SearchLectureForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (Validation.ObjectExist(Code_TextBox.Text, "Lecture"))
            {
                Lecture lecture = DatabaseConnection.Lecture_RawData(Code_TextBox.Text);
                string[] info = lecture.Info().Split('_');
                Name_Label.Text = "Title: " + info[1];
                Code_Label.Text = "Code: " + info[0];
                LectureType_Label.Text = "Lecture Type: " + info[2];
                Unit_Label.Text = "Units: " + info[3];
                DataTable dataTable = DatabaseConnection.ShowData("select * from Grades where LctCode = '" + Code_TextBox.Text + "'");
                Grade_DataGridView.DataSource = dataTable.DefaultView;
            }
            else
            {
                MessageBox.Show("Lecture Code: " + Code_TextBox.Text + " Not Exist!", "Data Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
