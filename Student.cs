﻿namespace Sama_System
{
    public class Student : Human
    {
        private string Field;

        public Student(string code, string name, string last_name, string field)
        {
            this.Code = code;
            this.Name = name;
            this.LastName = last_name;
            this.Field = field;
        }
            
        public void Add_toDataBase()
        {
            DatabaseConnection.ManipulateData("insert into Student values('" + this.Code + "','" + this.Name + "','" + this.LastName + "','" + this.Field + "')");
        }

        public string Info()
        {
            string info;
            info = this.Code + "_" + this.Name + "_" + this.LastName + "_" + this.Field;
            return (info);
        }
    }
}
