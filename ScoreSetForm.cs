﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class ScoreSetForm : Form
    {
        public ScoreSetForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (Validation.CodeExist(StCode_TextBox.Text, "Student") && Validation.CodeExist(TCode_TextBox.Text, "Teacher") && Validation.CodeExist(LctCode_TextBox.Text, "Lecture"))
            {
                if (Validation.MaxValue(int.Parse(Semester_TextBox.Text), 10) && Validation.MinValue(int.Parse(Semester_TextBox.Text), 1))
                {
                    if (Validation.ScoreValidation(float.Parse(Score_TextBox.Text)))
                    {
                        if (!Validation.ScoreExist(StCode_TextBox.Text, LctCode_TextBox.Text, int.Parse(Semester_TextBox.Text)))
                        {
                            Grades grade = new Grades(StCode_TextBox.Text, TCode_TextBox.Text, LctCode_TextBox.Text, int.Parse(Semester_TextBox.Text), float.Parse(Score_TextBox.Text));
                            grade.Add_toDatabase();
                            MessageBox.Show("Score Added Successfuly!", "Score Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            TCode_TextBox.Text = "";
                            LctCode_TextBox.Text = "";
                            Semester_TextBox.Text = "";
                            Score_TextBox.Text = "";
                        }
                        else
                        {
                            DialogResult rslt = MessageBox.Show("Student Has a Score in Selected Semester. Edit Score?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            if (rslt == DialogResult.No)
                            {
                                Semester_TextBox.Text = "";
                            }
                            if (rslt == DialogResult.Yes)
                            {
                                DatabaseConnection.ManipulateData("delete from Grades where StCode='" + StCode_TextBox.Text + "' and LctCode='" + LctCode_TextBox.Text + "' and Semester=" + int.Parse(Semester_TextBox.Text));
                                Grades grade = new Grades(StCode_TextBox.Text, TCode_TextBox.Text, LctCode_TextBox.Text, int.Parse(Semester_TextBox.Text), float.Parse(Score_TextBox.Text));
                                grade.Add_toDatabase();
                                MessageBox.Show("Score Added Successfuly!", "Score Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                TCode_TextBox.Text = "";
                                LctCode_TextBox.Text = "";
                                Semester_TextBox.Text = "";
                                Score_TextBox.Text = "";
                            }
                        }

                    }
                    else
                    {
                        MessageBox.Show("Score Must be From 0 to 20", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Semester Must be From 1 to 10", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                DialogResult rslt = MessageBox.Show("Please Enter Corret Codes For Each Field", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (rslt == DialogResult.OK)
                {
                    StCode_TextBox.Text = "";
                    TCode_TextBox.Text = "";
                    LctCode_TextBox.Text = "";
                    Semester_TextBox.Text = "";
                    Score_TextBox.Text = "";
                }
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            string StCode = StCode_TextBox.Text;
            DataTable dataTable = DatabaseConnection.ShowData("select * from Grades where StCode = '" + StCode + "'");
            Score_DataGridView.DataSource = dataTable.DefaultView;
        }
    }
}
