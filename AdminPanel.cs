﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class AdminPanel : Form
    {
        public AdminPanel()
        {
            InitializeComponent();
        }

        private void AdminPanel_Closing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void AdminPanel_Load(object sender, System.EventArgs e)
        {
            Username_StripMenu.Text = LoginForm.Uname;
            DataTable dataTable = DatabaseConnection.ShowData("select * from Users");
            Users_DataGridView.DataSource = dataTable.DefaultView;
            dataTable = DatabaseConnection.ShowData("select * from Student");
            Students_DataGridView.DataSource = dataTable.DefaultView;
            dataTable = DatabaseConnection.ShowData("select * from Teacher");
            Teachers_DataGridView.DataSource = dataTable.DefaultView;
        }

        private void userToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            NewUserForm Form = new NewUserForm();
            Form.ShowDialog();
        }

        private void Timer_Tick(object sender, System.EventArgs e)
        {
            DataTable dataTable = DatabaseConnection.ShowData("select * from Users");
            Users_DataGridView.DataSource = dataTable.DefaultView;
            dataTable = DatabaseConnection.ShowData("select * from Student");
            Students_DataGridView.DataSource = dataTable.DefaultView;
            dataTable = DatabaseConnection.ShowData("select * from Teacher");
            Teachers_DataGridView.DataSource = dataTable.DefaultView;
        }

        private void studentToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            NewStudentForm Form = new NewStudentForm();
            Form.ShowDialog();
        }

        private void teacherToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            NewTeacherForm Form = new NewTeacherForm();
            Form.ShowDialog();
        }

        private void lectureToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            NewLectureForm Form = new NewLectureForm();
            Form.ShowDialog();
        }

        private void amongStudentsToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            SearchStudentForm Form = new SearchStudentForm();
            Form.ShowDialog();
        }

        private void amongTeachersToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            SearchTeacherForm Form = new SearchTeacherForm();
            Form.ShowDialog();
        }

        private void logoutToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void amongLecturesToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            SearchLectureForm Form = new SearchLectureForm();
            Form.ShowDialog();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            ChangePasswordForm Form = new ChangePasswordForm();
            Form.ShowDialog();
        }

        private void usersToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            EditUesrForm Form = new EditUesrForm();
            Form.ShowDialog();
        }

        private void studentRecordsToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            EditStudentForm Form = new EditStudentForm();
            Form.ShowDialog();
        }

        private void teacherRecordsToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            EditTeacherForm Form = new EditTeacherForm();
            Form.ShowDialog();
        }

        private void lectureRecordsToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            EditLectureForm Form = new EditLectureForm();
            Form.ShowDialog();
        }

        private void scoreAStudentToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            ScoreSetForm Form = new ScoreSetForm();
            Form.ShowDialog();
        }

        private void backupToolStripMenuItem1_Click(object sender, System.EventArgs e)
        {
            FolderBrowserDialog.ShowDialog();
            string path = FolderBrowserDialog.SelectedPath.ToString() + "\\University." + DateTime.Now.ToString("yyyy.MM.dd.H.mm") + ".bak";
            DatabaseConnection.BackupData(path);
        }

        private void restoreLastBackupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog.ShowDialog();
            DialogResult rslt = MessageBox.Show("Are you sure you want to restore data?\nAll data will be replaced and program will be rebooted!", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (rslt == DialogResult.Yes)
            {
                string path = OpenFileDialog.FileName;
                this.Close();
                DatabaseConnection.RestoreData(path);
            }
        }

        private void AdminPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.loginForm.Show();
        }
    }
}