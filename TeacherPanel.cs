﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class TeacherPanel : Form
    {
        public TeacherPanel()
        {
            InitializeComponent();
        }

        private void TeacherPanel_Load(object sender, EventArgs e)
        {
            Username_StripMenu.Text = LoginForm.Uname;
            DataTable dataTable = DatabaseConnection.ShowData("select * from Student");
            Student_GridView.DataSource = dataTable.DefaultView;
            dataTable = DatabaseConnection.ShowData("select * from Lecture");
            Lecture_DataGridView.DataSource = dataTable.DefaultView;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            DataTable dataTable = DatabaseConnection.ShowData("select * from Student");
            Student_GridView.DataSource = dataTable.DefaultView;
            dataTable = DatabaseConnection.ShowData("select * from Lecture");
            Lecture_DataGridView.DataSource = dataTable.DefaultView;
        }

        private void scoreAStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ScoreSetForm Form = new ScoreSetForm();
            Form.ShowDialog();
        }

        private void TeacherPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.loginForm.Show();
        }

        private void newStudentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewStudentForm Form = new NewStudentForm();
            Form.ShowDialog();
        }

        private void newLectureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewLectureForm Form = new NewLectureForm();
            Form.ShowDialog();
        }

        private void amongStudentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchStudentForm Form = new SearchStudentForm();
            Form.ShowDialog();
        }

        private void amongTeachersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchTeacherForm Form = new SearchTeacherForm();
            Form.ShowDialog();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void amongLecturesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchLectureForm Form = new SearchLectureForm();
            Form.ShowDialog();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePasswordForm Form = new ChangePasswordForm();
            Form.ShowDialog();
        }
    }
}
