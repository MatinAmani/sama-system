﻿namespace Sama_System
{
    partial class NewStudentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewStudentForm));
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.Name_TextBox = new System.Windows.Forms.TextBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.LastName_TextBox = new System.Windows.Forms.TextBox();
			this.Code_TextBox = new System.Windows.Forms.TextBox();
			this.Field_ComboBox = new System.Windows.Forms.ComboBox();
			this.Add_Button = new System.Windows.Forms.Button();
			this.Cancel_Button = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(0, 11);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(74, 25);
			this.label1.TabIndex = 0;
			this.label1.Text = "Name:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(0, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(121, 25);
			this.label2.TabIndex = 1;
			this.label2.Text = "Last Name:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(0, 85);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(69, 25);
			this.label3.TabIndex = 2;
			this.label3.Text = "Code:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(0, 122);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(65, 25);
			this.label4.TabIndex = 3;
			this.label4.Text = "Field:";
			// 
			// Name_TextBox
			// 
			this.Name_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Name_TextBox.Location = new System.Drawing.Point(127, 8);
			this.Name_TextBox.Name = "Name_TextBox";
			this.Name_TextBox.Size = new System.Drawing.Size(197, 31);
			this.Name_TextBox.TabIndex = 4;
			// 
			// panel1
			// 
			this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
			this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.panel1.Location = new System.Drawing.Point(338, 45);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(100, 100);
			this.panel1.TabIndex = 5;
			// 
			// LastName_TextBox
			// 
			this.LastName_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.LastName_TextBox.Location = new System.Drawing.Point(127, 45);
			this.LastName_TextBox.Name = "LastName_TextBox";
			this.LastName_TextBox.Size = new System.Drawing.Size(197, 31);
			this.LastName_TextBox.TabIndex = 6;
			// 
			// Code_TextBox
			// 
			this.Code_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Code_TextBox.Location = new System.Drawing.Point(127, 82);
			this.Code_TextBox.Name = "Code_TextBox";
			this.Code_TextBox.Size = new System.Drawing.Size(197, 31);
			this.Code_TextBox.TabIndex = 7;
			this.Code_TextBox.TextChanged += new System.EventHandler(this.Code_TextBox_TextChanged);
			// 
			// Field_ComboBox
			// 
			this.Field_ComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Field_ComboBox.FormattingEnabled = true;
			this.Field_ComboBox.Items.AddRange(new object[] {
            "Aerospace",
            "ChemicalEng.",
            "Civil",
            "ComputerEng.",
            "ComputerSci.",
            "Industrial",
            "IT",
            "Material",
            "Mechanical",
            "Optical"});
			this.Field_ComboBox.Location = new System.Drawing.Point(127, 119);
			this.Field_ComboBox.Name = "Field_ComboBox";
			this.Field_ComboBox.Size = new System.Drawing.Size(197, 33);
			this.Field_ComboBox.Sorted = true;
			this.Field_ComboBox.TabIndex = 8;
			// 
			// Add_Button
			// 
			this.Add_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Add_Button.Location = new System.Drawing.Point(122, 158);
			this.Add_Button.Name = "Add_Button";
			this.Add_Button.Size = new System.Drawing.Size(100, 35);
			this.Add_Button.TabIndex = 9;
			this.Add_Button.Text = "Add";
			this.Add_Button.UseVisualStyleBackColor = true;
			this.Add_Button.Click += new System.EventHandler(this.Add_Button_Click);
			// 
			// Cancel_Button
			// 
			this.Cancel_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Cancel_Button.Location = new System.Drawing.Point(228, 158);
			this.Cancel_Button.Name = "Cancel_Button";
			this.Cancel_Button.Size = new System.Drawing.Size(100, 35);
			this.Cancel_Button.TabIndex = 10;
			this.Cancel_Button.Text = "Cancel";
			this.Cancel_Button.UseVisualStyleBackColor = true;
			this.Cancel_Button.Click += new System.EventHandler(this.Cancel_Button_Click);
			// 
			// NewStudentForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(450, 200);
			this.Controls.Add(this.Cancel_Button);
			this.Controls.Add(this.Add_Button);
			this.Controls.Add(this.Field_ComboBox);
			this.Controls.Add(this.Code_TextBox);
			this.Controls.Add(this.LastName_TextBox);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.Name_TextBox);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "NewStudentForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "New Student";
			this.Load += new System.EventHandler(this.NewStudentForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Name_TextBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox LastName_TextBox;
        private System.Windows.Forms.TextBox Code_TextBox;
        private System.Windows.Forms.ComboBox Field_ComboBox;
        private System.Windows.Forms.Button Add_Button;
        private System.Windows.Forms.Button Cancel_Button;
    }
}