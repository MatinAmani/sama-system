﻿namespace Sama_System
{
    partial class SearchStudentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchStudentForm));
            this.Records_Panel = new System.Windows.Forms.Panel();
            this.Grade_Label = new System.Windows.Forms.Label();
            this.Grade_DataGridView = new System.Windows.Forms.DataGridView();
            this.Field_Label = new System.Windows.Forms.Label();
            this.Code_Label = new System.Windows.Forms.Label();
            this.FullName_Label = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Search_Panel = new System.Windows.Forms.Panel();
            this.CancelButton = new System.Windows.Forms.Button();
            this.SearchButton = new System.Windows.Forms.Button();
            this.Code_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Records_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grade_DataGridView)).BeginInit();
            this.Search_Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Records_Panel
            // 
            this.Records_Panel.Controls.Add(this.Grade_Label);
            this.Records_Panel.Controls.Add(this.Grade_DataGridView);
            this.Records_Panel.Controls.Add(this.Field_Label);
            this.Records_Panel.Controls.Add(this.Code_Label);
            this.Records_Panel.Controls.Add(this.FullName_Label);
            this.Records_Panel.Location = new System.Drawing.Point(15, 118);
            this.Records_Panel.Name = "Records_Panel";
            this.Records_Panel.Size = new System.Drawing.Size(597, 320);
            this.Records_Panel.TabIndex = 0;
            // 
            // Grade_Label
            // 
            this.Grade_Label.AutoSize = true;
            this.Grade_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Grade_Label.Location = new System.Drawing.Point(3, 115);
            this.Grade_Label.Name = "Grade_Label";
            this.Grade_Label.Size = new System.Drawing.Size(88, 25);
            this.Grade_Label.TabIndex = 6;
            this.Grade_Label.Text = "Grades:";
            // 
            // Grade_DataGridView
            // 
            this.Grade_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Grade_DataGridView.Location = new System.Drawing.Point(0, 143);
            this.Grade_DataGridView.Name = "Grade_DataGridView";
            this.Grade_DataGridView.ReadOnly = true;
            this.Grade_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Grade_DataGridView.Size = new System.Drawing.Size(597, 177);
            this.Grade_DataGridView.TabIndex = 3;
            // 
            // Field_Label
            // 
            this.Field_Label.AutoSize = true;
            this.Field_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Field_Label.Location = new System.Drawing.Point(3, 56);
            this.Field_Label.Name = "Field_Label";
            this.Field_Label.Size = new System.Drawing.Size(65, 25);
            this.Field_Label.TabIndex = 5;
            this.Field_Label.Text = "Field:";
            // 
            // Code_Label
            // 
            this.Code_Label.AutoSize = true;
            this.Code_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Code_Label.Location = new System.Drawing.Point(306, 11);
            this.Code_Label.Name = "Code_Label";
            this.Code_Label.Size = new System.Drawing.Size(69, 25);
            this.Code_Label.TabIndex = 4;
            this.Code_Label.Text = "Code:";
            // 
            // FullName_Label
            // 
            this.FullName_Label.AutoSize = true;
            this.FullName_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FullName_Label.Location = new System.Drawing.Point(3, 11);
            this.FullName_Label.Name = "FullName_Label";
            this.FullName_Label.Size = new System.Drawing.Size(74, 25);
            this.FullName_Label.TabIndex = 3;
            this.FullName_Label.Text = "Name:";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Location = new System.Drawing.Point(512, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(100, 100);
            this.panel1.TabIndex = 3;
            // 
            // Search_Panel
            // 
            this.Search_Panel.Controls.Add(this.CancelButton);
            this.Search_Panel.Controls.Add(this.SearchButton);
            this.Search_Panel.Controls.Add(this.Code_TextBox);
            this.Search_Panel.Controls.Add(this.label1);
            this.Search_Panel.Location = new System.Drawing.Point(15, 12);
            this.Search_Panel.Name = "Search_Panel";
            this.Search_Panel.Size = new System.Drawing.Size(491, 100);
            this.Search_Panel.TabIndex = 4;
            // 
            // CancelButton
            // 
            this.CancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelButton.Location = new System.Drawing.Point(387, 53);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(94, 31);
            this.CancelButton.TabIndex = 7;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // SearchButton
            // 
            this.SearchButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchButton.Location = new System.Drawing.Point(387, 16);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(94, 31);
            this.SearchButton.TabIndex = 6;
            this.SearchButton.Text = "Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // Code_TextBox
            // 
            this.Code_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Code_TextBox.Location = new System.Drawing.Point(164, 35);
            this.Code_TextBox.Name = "Code_TextBox";
            this.Code_TextBox.Size = new System.Drawing.Size(217, 31);
            this.Code_TextBox.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Student Code:";
            // 
            // SearchStudentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(627, 450);
            this.Controls.Add(this.Search_Panel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Records_Panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SearchStudentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search Students";
            this.Records_Panel.ResumeLayout(false);
            this.Records_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grade_DataGridView)).EndInit();
            this.Search_Panel.ResumeLayout(false);
            this.Search_Panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Records_Panel;
        private System.Windows.Forms.Label Grade_Label;
        private System.Windows.Forms.DataGridView Grade_DataGridView;
        private System.Windows.Forms.Label Field_Label;
        private System.Windows.Forms.Label Code_Label;
        private System.Windows.Forms.Label FullName_Label;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel Search_Panel;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.TextBox Code_TextBox;
        private System.Windows.Forms.Label label1;
    }
}