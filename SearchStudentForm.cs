﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class SearchStudentForm : Form
    {
        public SearchStudentForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (Validation.ObjectExist(Code_TextBox.Text, "Student"))
            {
                Student student = DatabaseConnection.Student_RawData(Code_TextBox.Text);
                string[] info = student.Info().Split('_');
                FullName_Label.Text = "Name: " + (info[1] + " " + info[2]);
                Code_Label.Text = "Code: " + info[0];
                Field_Label.Text = "Field: " + info[3];
                DataTable dataTable = DatabaseConnection.ShowData("select * from Grades where StCode = '" + Code_TextBox.Text + "'");
                Grade_DataGridView.DataSource = dataTable.DefaultView;
            }
            else
            {
                MessageBox.Show("Student Code: " + Code_TextBox.Text + " Not Exist!", "Data Not Exist", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
