﻿using System;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class EditUesrForm : Form
    {
        public EditUesrForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            if (Validation.UserExist(Username_TextBox.Text))
            {
                EditButton.Enabled = true;
                DeleteButton.Enabled = true;
                DiscardButton.Enabled = true;
                UsernameRslt_TextBox.Enabled = false;
                Password_TextBox.Enabled = false;
                UserType_ComboBox.Enabled = false;
                User user = DatabaseConnection.User_RawData(Username_TextBox.Text);
                string[] info = user.Info().Split(' ');
                UsernameRslt_TextBox.Text = info[0];
                Password_TextBox.Text = info[1];
                UserType_ComboBox.Text = info[2];
            }
            else
            {
                MessageBox.Show("Username: " + Username_TextBox.Text + " Does Not Exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            SaveButton.Enabled = true;
            UsernameRslt_TextBox.Enabled = true;
            Password_TextBox.Enabled = true;
            UserType_ComboBox.Enabled = true;
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            DialogResult rslt = MessageBox.Show("Are You Sure You Want to Delete User: " + Username_TextBox.Text + "?", "User Will be Deleted", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (rslt == DialogResult.Yes)
            {
                DatabaseConnection.ManipulateData("delete from Users where UserName='" + Username_TextBox.Text + "'");
                EditButton.Enabled = false;
                DeleteButton.Enabled = false;
                SaveButton.Enabled = false;
                DiscardButton.Enabled = false;
                Username_TextBox.Text = "";
                UsernameRslt_TextBox.Text = "";
                Password_TextBox.Text = "";
                UserType_ComboBox.Text = "";
                UsernameRslt_TextBox.Enabled = false;
                Password_TextBox.Enabled = false;
                UserType_ComboBox.Enabled = false;
                MessageBox.Show("User Deleted Successfuly!", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void DiscardButton_Click(object sender, EventArgs e)
        {
            DialogResult rslt = MessageBox.Show("Discard Changes?", "Unsaved Settings", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (rslt == DialogResult.Yes)
            {
                SaveButton.Enabled = false;
                UsernameRslt_TextBox.Enabled = false;
                Password_TextBox.Enabled = false;
                UserType_ComboBox.Enabled = false;
                User user = DatabaseConnection.User_RawData(Username_TextBox.Text);
                string[] info = user.Info().Split(' ');
                UsernameRslt_TextBox.Text = info[0];
                Password_TextBox.Text = info[1];
                UserType_ComboBox.Text = info[2];
            }
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            DatabaseConnection.ManipulateData("update Users set UserName='" + UsernameRslt_TextBox.Text + "', UserPass='" + Password_TextBox.Text + "', UserType='" + UserType_ComboBox.SelectedItem.ToString() + "'where UserName='" + Username_TextBox.Text + "'");
            MessageBox.Show("Records Updated", "Done!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Username_TextBox.Text = "";
            UsernameRslt_TextBox.Text = "";
            Password_TextBox.Text = "";
            UserType_ComboBox.Text = "";
            EditButton.Enabled = false;
            DeleteButton.Enabled = false;
            SaveButton.Enabled = false;
            DiscardButton.Enabled = false;
            UsernameRslt_TextBox.Enabled = false;
            Password_TextBox.Enabled = false;
            UserType_ComboBox.Enabled = false;
        }
    }
}
