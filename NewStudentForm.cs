﻿using System;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class NewStudentForm : Form
    {
        public NewStudentForm()
        {
            InitializeComponent();
        }

        private void Cancel_Button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Add_Button_Click(object sender, EventArgs e)
        {
            if (!Validation.Null(Name_TextBox.Text) && !Validation.Null(LastName_TextBox.Text) && !Validation.Null(Code_TextBox.Text) && !Validation.Null(Field_ComboBox.Text.ToString()))
            {
                if (Validation.CodeUniqueness(Code_TextBox.Text, "Student"))
                {
                    if (Validation.MaxLength(Code_TextBox.Text, 10))
                    {
                        Student student = new Student(Code_TextBox.Text, Name_TextBox.Text, LastName_TextBox.Text, Field_ComboBox.SelectedItem.ToString());
                        student.Add_toDataBase();
                        this.Close();
                    }
                    else
                    {
                        DialogResult rslt = MessageBox.Show("Code Must be Less Than 10 Digits!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        if (rslt == DialogResult.OK)
                        {
                            Code_TextBox.Text = "";
                        }
                    }
                }
                else
                {
                    string str = Code_TextBox.Text;
                    DialogResult rslt = MessageBox.Show("Student With Code: " + str + " Already Exists!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (rslt == DialogResult.OK)
                    {
                        Name_TextBox.Text = "";
                        LastName_TextBox.Text = "";
                        Code_TextBox.Text = "";
                        Field_ComboBox.Text = "";
                    }
                }
            }
            else
            {
                MessageBox.Show("Please Fill All The Fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Code_TextBox_TextChanged(object sender, EventArgs e)
        {
            string value = Code_TextBox.Text;
            if (!Validation.CodeUniqueness(value, "Student"))
            {
                value = Code_TextBox.Text;
                MessageBox.Show("Student With Code: " + value + " Already Exists!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (!Validation.MaxLength(value, 10))
            {
                MessageBox.Show("Code Must Have 10 Digits!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

		private void NewStudentForm_Load(object sender, EventArgs e)
		{

		}
	}
}
