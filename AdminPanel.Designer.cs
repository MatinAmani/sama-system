﻿namespace Sama_System
{
    partial class AdminPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teacherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lectureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teacherRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lectureRecordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.amongStudentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.amongTeachersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.amongLecturesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreLastBackupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scoreOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scoreAStudentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Username_StripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Users_DataGridView = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Students_DataGridView = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Teachers_DataGridView = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.FolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Users_DataGridView)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Students_DataGridView)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Teachers_DataGridView)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.editToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.reportToolStripMenuItem,
            this.accountToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1354, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userToolStripMenuItem,
            this.studentToolStripMenuItem,
            this.teacherToolStripMenuItem,
            this.lectureToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(51, 24);
            this.newToolStripMenuItem.Text = "New";
            // 
            // userToolStripMenuItem
            // 
            this.userToolStripMenuItem.Name = "userToolStripMenuItem";
            this.userToolStripMenuItem.Size = new System.Drawing.Size(129, 24);
            this.userToolStripMenuItem.Text = "User";
            this.userToolStripMenuItem.Click += new System.EventHandler(this.userToolStripMenuItem_Click);
            // 
            // studentToolStripMenuItem
            // 
            this.studentToolStripMenuItem.Name = "studentToolStripMenuItem";
            this.studentToolStripMenuItem.Size = new System.Drawing.Size(129, 24);
            this.studentToolStripMenuItem.Text = "Student";
            this.studentToolStripMenuItem.Click += new System.EventHandler(this.studentToolStripMenuItem_Click);
            // 
            // teacherToolStripMenuItem
            // 
            this.teacherToolStripMenuItem.Name = "teacherToolStripMenuItem";
            this.teacherToolStripMenuItem.Size = new System.Drawing.Size(129, 24);
            this.teacherToolStripMenuItem.Text = "Teacher";
            this.teacherToolStripMenuItem.Click += new System.EventHandler(this.teacherToolStripMenuItem_Click);
            // 
            // lectureToolStripMenuItem
            // 
            this.lectureToolStripMenuItem.Name = "lectureToolStripMenuItem";
            this.lectureToolStripMenuItem.Size = new System.Drawing.Size(129, 24);
            this.lectureToolStripMenuItem.Text = "Lecture";
            this.lectureToolStripMenuItem.Click += new System.EventHandler(this.lectureToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usersToolStripMenuItem,
            this.studentRecordsToolStripMenuItem,
            this.teacherRecordsToolStripMenuItem,
            this.lectureRecordsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(47, 24);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(186, 24);
            this.usersToolStripMenuItem.Text = "Users";
            this.usersToolStripMenuItem.Click += new System.EventHandler(this.usersToolStripMenuItem_Click);
            // 
            // studentRecordsToolStripMenuItem
            // 
            this.studentRecordsToolStripMenuItem.Name = "studentRecordsToolStripMenuItem";
            this.studentRecordsToolStripMenuItem.Size = new System.Drawing.Size(186, 24);
            this.studentRecordsToolStripMenuItem.Text = "Student Records";
            this.studentRecordsToolStripMenuItem.Click += new System.EventHandler(this.studentRecordsToolStripMenuItem_Click);
            // 
            // teacherRecordsToolStripMenuItem
            // 
            this.teacherRecordsToolStripMenuItem.Name = "teacherRecordsToolStripMenuItem";
            this.teacherRecordsToolStripMenuItem.Size = new System.Drawing.Size(186, 24);
            this.teacherRecordsToolStripMenuItem.Text = "Teacher Records";
            this.teacherRecordsToolStripMenuItem.Click += new System.EventHandler(this.teacherRecordsToolStripMenuItem_Click);
            // 
            // lectureRecordsToolStripMenuItem
            // 
            this.lectureRecordsToolStripMenuItem.Name = "lectureRecordsToolStripMenuItem";
            this.lectureRecordsToolStripMenuItem.Size = new System.Drawing.Size(186, 24);
            this.lectureRecordsToolStripMenuItem.Text = "Lecture Records";
            this.lectureRecordsToolStripMenuItem.Click += new System.EventHandler(this.lectureRecordsToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.amongStudentsToolStripMenuItem,
            this.amongTeachersToolStripMenuItem,
            this.amongLecturesToolStripMenuItem});
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(65, 24);
            this.searchToolStripMenuItem.Text = "Search";
            // 
            // amongStudentsToolStripMenuItem
            // 
            this.amongStudentsToolStripMenuItem.Name = "amongStudentsToolStripMenuItem";
            this.amongStudentsToolStripMenuItem.Size = new System.Drawing.Size(188, 24);
            this.amongStudentsToolStripMenuItem.Text = "Among Students";
            this.amongStudentsToolStripMenuItem.Click += new System.EventHandler(this.amongStudentsToolStripMenuItem_Click);
            // 
            // amongTeachersToolStripMenuItem
            // 
            this.amongTeachersToolStripMenuItem.Name = "amongTeachersToolStripMenuItem";
            this.amongTeachersToolStripMenuItem.Size = new System.Drawing.Size(188, 24);
            this.amongTeachersToolStripMenuItem.Text = "Among Teachers";
            this.amongTeachersToolStripMenuItem.Click += new System.EventHandler(this.amongTeachersToolStripMenuItem_Click);
            // 
            // amongLecturesToolStripMenuItem
            // 
            this.amongLecturesToolStripMenuItem.Name = "amongLecturesToolStripMenuItem";
            this.amongLecturesToolStripMenuItem.Size = new System.Drawing.Size(188, 24);
            this.amongLecturesToolStripMenuItem.Text = "Among Lectures";
            this.amongLecturesToolStripMenuItem.Click += new System.EventHandler(this.amongLecturesToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backupToolStripMenuItem,
            this.scoreOptionsToolStripMenuItem});
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(56, 24);
            this.reportToolStripMenuItem.Text = "Tools";
            // 
            // backupToolStripMenuItem
            // 
            this.backupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backupToolStripMenuItem1,
            this.restoreLastBackupToolStripMenuItem});
            this.backupToolStripMenuItem.Name = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.backupToolStripMenuItem.Text = "Backup Tools";
            // 
            // backupToolStripMenuItem1
            // 
            this.backupToolStripMenuItem1.Name = "backupToolStripMenuItem1";
            this.backupToolStripMenuItem1.Size = new System.Drawing.Size(224, 24);
            this.backupToolStripMenuItem1.Text = "Backup";
            this.backupToolStripMenuItem1.Click += new System.EventHandler(this.backupToolStripMenuItem1_Click);
            // 
            // restoreLastBackupToolStripMenuItem
            // 
            this.restoreLastBackupToolStripMenuItem.Name = "restoreLastBackupToolStripMenuItem";
            this.restoreLastBackupToolStripMenuItem.Size = new System.Drawing.Size(224, 24);
            this.restoreLastBackupToolStripMenuItem.Text = "Restore From Backups";
            this.restoreLastBackupToolStripMenuItem.Click += new System.EventHandler(this.restoreLastBackupToolStripMenuItem_Click);
            // 
            // scoreOptionsToolStripMenuItem
            // 
            this.scoreOptionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scoreAStudentToolStripMenuItem});
            this.scoreOptionsToolStripMenuItem.Name = "scoreOptionsToolStripMenuItem";
            this.scoreOptionsToolStripMenuItem.Size = new System.Drawing.Size(180, 24);
            this.scoreOptionsToolStripMenuItem.Text = "Score Options";
            // 
            // scoreAStudentToolStripMenuItem
            // 
            this.scoreAStudentToolStripMenuItem.Name = "scoreAStudentToolStripMenuItem";
            this.scoreAStudentToolStripMenuItem.Size = new System.Drawing.Size(182, 24);
            this.scoreAStudentToolStripMenuItem.Text = "Score a Student";
            this.scoreAStudentToolStripMenuItem.Click += new System.EventHandler(this.scoreAStudentToolStripMenuItem_Click);
            // 
            // accountToolStripMenuItem
            // 
            this.accountToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Username_StripMenu,
            this.changePasswordToolStripMenuItem,
            this.logOutToolStripMenuItem});
            this.accountToolStripMenuItem.Name = "accountToolStripMenuItem";
            this.accountToolStripMenuItem.Size = new System.Drawing.Size(75, 24);
            this.accountToolStripMenuItem.Text = "Account";
            // 
            // Username_StripMenu
            // 
            this.Username_StripMenu.Enabled = false;
            this.Username_StripMenu.Name = "Username_StripMenu";
            this.Username_StripMenu.Size = new System.Drawing.Size(193, 24);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.changePasswordToolStripMenuItem.Text = "Change Password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(193, 24);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Users_DataGridView);
            this.panel1.Location = new System.Drawing.Point(4, 99);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(345, 450);
            this.panel1.TabIndex = 1;
            // 
            // Users_DataGridView
            // 
            this.Users_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Users_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Users_DataGridView.Location = new System.Drawing.Point(0, 0);
            this.Users_DataGridView.Name = "Users_DataGridView";
            this.Users_DataGridView.ReadOnly = true;
            this.Users_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Users_DataGridView.Size = new System.Drawing.Size(345, 450);
            this.Users_DataGridView.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Students_DataGridView);
            this.panel2.Location = new System.Drawing.Point(355, 99);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(445, 450);
            this.panel2.TabIndex = 2;
            // 
            // Students_DataGridView
            // 
            this.Students_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Students_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Students_DataGridView.Location = new System.Drawing.Point(0, 0);
            this.Students_DataGridView.Name = "Students_DataGridView";
            this.Students_DataGridView.ReadOnly = true;
            this.Students_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Students_DataGridView.Size = new System.Drawing.Size(445, 450);
            this.Students_DataGridView.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.Teachers_DataGridView);
            this.panel3.Location = new System.Drawing.Point(806, 99);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(545, 450);
            this.panel3.TabIndex = 2;
            // 
            // Teachers_DataGridView
            // 
            this.Teachers_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Teachers_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Teachers_DataGridView.Location = new System.Drawing.Point(0, 0);
            this.Teachers_DataGridView.Name = "Teachers_DataGridView";
            this.Teachers_DataGridView.ReadOnly = true;
            this.Teachers_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Teachers_DataGridView.Size = new System.Drawing.Size(545, 450);
            this.Teachers_DataGridView.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel4.Controls.Add(this.label1);
            this.panel4.Location = new System.Drawing.Point(4, 31);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(345, 62);
            this.panel4.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(138, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Users";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Chartreuse;
            this.panel5.Controls.Add(this.label2);
            this.panel5.Location = new System.Drawing.Point(355, 31);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(445, 62);
            this.panel5.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(172, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 28);
            this.label2.TabIndex = 1;
            this.label2.Text = "Students";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Yellow;
            this.panel6.Controls.Add(this.label3);
            this.panel6.Location = new System.Drawing.Point(806, 31);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(545, 62);
            this.panel6.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(221, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 28);
            this.label3.TabIndex = 2;
            this.label3.Text = "Teachers";
            // 
            // Timer
            // 
            this.Timer.Enabled = true;
            this.Timer.Interval = 2000;
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // FolderBrowserDialog
            // 
            this.FolderBrowserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.FolderBrowserDialog.SelectedPath = "F:\\Database Backups";
            // 
            // OpenFileDialog
            // 
            this.OpenFileDialog.Filter = "Backup File|*.bak";
            this.OpenFileDialog.Title = "Select Backup File";
            // 
            // AdminPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1354, 561);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "AdminPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admins Panel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminPanel_FormClosing);
            this.Load += new System.EventHandler(this.AdminPanel_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Users_DataGridView)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Students_DataGridView)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Teachers_DataGridView)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teacherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lectureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem teacherRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lectureRecordsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem restoreLastBackupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem amongStudentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem amongTeachersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem amongLecturesToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView Users_DataGridView;
        private System.Windows.Forms.DataGridView Students_DataGridView;
        private System.Windows.Forms.DataGridView Teachers_DataGridView;
        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.ToolStripMenuItem accountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Username_StripMenu;
        private System.Windows.Forms.ToolStripMenuItem scoreOptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scoreAStudentToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog FolderBrowserDialog;
        private System.Windows.Forms.OpenFileDialog OpenFileDialog;
    }
}