﻿using System;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class NewLectureForm : Form
    {
        public NewLectureForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (Validation.CodeUniqueness(Code_TextBox.Text, "Lecture"))
            {
                if (Validation.CodeLength(Code_TextBox.Text, 10))
                {
                    if (Validation.UnitValidation(int.Parse(Unit_TextBox.Text), 1, 6))
                    {
                        if (!Validation.Null(Name_TextBox.Text) && !Validation.Null(LectureType_ComboBox.SelectedItem.ToString()) && !Validation.Null(Unit_TextBox.Text))
                        {
                            Lecture lecture = new Lecture(Code_TextBox.Text, Name_TextBox.Text, LectureType_ComboBox.SelectedItem.ToString(), int.Parse(Unit_TextBox.Text));
                            lecture.Add_toDatabase();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Please Fill All The Fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Units Must be between 1 and 6", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    DialogResult rslt = MessageBox.Show("Code Must be 10 Digits!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (rslt == DialogResult.OK)
                    {
                        Code_TextBox.Text = "";
                    }
                }
            }
            else
            {
                string str = Code_TextBox.Text;
                DialogResult rslt = MessageBox.Show("Student With Code: " + str + " Already Exists!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (rslt == DialogResult.OK)
                {
                    Code_TextBox.Text = "";
                    Name_TextBox.Text = "";
                    LectureType_ComboBox.Text = "";
                    Unit_TextBox.Text = "";
                }
            }
        }
    }
}
