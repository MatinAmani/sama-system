﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class ChangePasswordForm : Form
    {
        public ChangePasswordForm()
        {
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConStr1"].ConnectionString);
            sqlConnection.Open();
            SqlCommand sqlCommand = new SqlCommand("select * from Users where UserName=N'" + LoginForm.Uname + "' and UserPass=N'" + OldPass_TextBox.Text + "'", sqlConnection);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            if (sqlDataReader.Read())
            {
                if (!Validation.Null(NewPass_TextBox.Text) && !Validation.Null(Confirm_TextBox.Text))
                {
                    if (Validation.PasswordConfirm(NewPass_TextBox.Text, Confirm_TextBox.Text))
                    {
                        DatabaseConnection.ManipulateData("update Users set UserPass='" + NewPass_TextBox.Text + "' where UserName='" + LoginForm.Uname + "'");
                        MessageBox.Show("Password Updated", "Done!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Please Confirm Your Password", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Confirm_TextBox.Text = "";
                    }
                }
                else
                {
                    MessageBox.Show("Please Fill All The Fields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Your Password is Incorrect!", "Incorrect Password", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
