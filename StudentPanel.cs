﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class StudentPanel : Form
    {
        public StudentPanel()
        {
            InitializeComponent();
        }

        private void StudentPanel_Load(object sender, EventArgs e)
        {
            Student student = DatabaseConnection.Student_RawData(LoginForm.Uname);
            string[] info = student.Info().Split('_');
            Username_StripMenu.Text = info[1] + " " + info[2];
            DataTable dataTable = DatabaseConnection.ShowData("select * from Grades where StCode = '" + LoginForm.Uname + "'");
            Grades_DataGridView.DataSource = dataTable.DefaultView;
        }

        private void StudentPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.loginForm.Show();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            DataTable dataTable = DatabaseConnection.ShowData("select * from Grades where StCode = '" + LoginForm.Uname + "'");
            Grades_DataGridView.DataSource = dataTable.DefaultView;
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePasswordForm Form = new ChangePasswordForm();
            Form.ShowDialog();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
