﻿using System;
using System.Windows.Forms;

namespace Sama_System
{
    public partial class NewUserForm : Form
    {
        public NewUserForm()
        {
            InitializeComponent();
        }

        private void Username_TextBox_TextChanged(object sender, EventArgs e)
        {
            if (!Validation.UsernameUniqueness(Username_TextBox.Text))
            {
                MessageBox.Show("Username Taken", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (Validation.ObjectExist(Username_TextBox.Text, "Student"))
            {
                UserType_ComboBox.SelectedItem = "Student";
                UserType_ComboBox.Enabled = false;
            }
            else
            {
                UserType_ComboBox.Text = "";
                UserType_ComboBox.Enabled = true;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (Validation.UsernameUniqueness(Username_TextBox.Text))
            {
                if (!Validation.Null(Password_TextBox.Text))
                {
                    if (Validation.PasswordConfirm(Password_TextBox.Text, Confirm_TextBox.Text))
                    {
                        if (!Validation.Null(UserType_ComboBox.SelectedItem.ToString()))
                        {
                            User user = new User(Username_TextBox.Text, Password_TextBox.Text, UserType_ComboBox.SelectedItem.ToString());
                            user.Add_toDatabase();
                            this.Close();
                        }
                        else
                        {
                            DialogResult rslt = MessageBox.Show("Please Select User Type", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            if (rslt == DialogResult.OK)
                            {
                                UserType_ComboBox.Text = "";
                            }
                        }
                    }
                    else
                    {
                        DialogResult rslt = MessageBox.Show("Please Confirm Your Password", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        if (rslt == DialogResult.OK)
                        {
                            Confirm_TextBox.Text = "";
                        }
                    }
                }
                else
                {
                    DialogResult rslt = MessageBox.Show("Please Set a Password", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                DialogResult rslt = MessageBox.Show("Username Taken. Try a Different Username", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (rslt == DialogResult.OK)
                {
                    Username_TextBox.Text = "";
                    Password_TextBox.Text = "";
                    Confirm_TextBox.Text = "";
                    UserType_ComboBox.Text = "";
                }
            }
        }
    }
}
